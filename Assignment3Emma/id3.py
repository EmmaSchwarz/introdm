import math
import operator
import pandas as pd
import numpy as np

column_names = ['Sepal Length', 'Sepal Width', 'Petal Length', 'Petal Width', 'Class']
data_set = pd.read_csv('iris.data.txt', header = None, names = column_names)


training_set = data_set[0:40].append(data_set[50:90]).append(data_set[100:140])

test_set = data_set[40:50].append(data_set[90:100]).append(data_set[140:150])

training_default = training_set['Class']
most = training_default.mode()
choose_default = most[0]

features = data_set.columns[:4]


def probability_of_classes(df,column):
    probability = df.groupby(column).size().div(len(df))
    return probability

def entropy_of_classes(df,column):
    total_probability = probability_of_classes(df, column)
    entropy = -sum([p*math.log(p)/math.log(2.0) for p in total_probability])
    return entropy


#Adapted from http://www.onlamp.com/pub/a/python/2006/02/09/ai_decision_trees.html?page=4

def information_gain(df, attr, target_attr):
    unique_vals = df[attr].unique()
    val_freq = df[attr].value_counts()
    subset_entropy = 0.0
    df_length = len(df)

    for val in unique_vals:
        val_prob = val_freq[val]/df_length
        df_subset = df[df[attr] == val]
        subset_entropy += val_prob * entropy_of_classes(df_subset,target_attr)

    return (entropy_of_classes(df, target_attr) - subset_entropy)

def find_split(df,target_attr):
    max_info_gain = 0.0
    max_attr = ''
    collist = df.columns.tolist()
    collist.remove(target_attr)
    df_attr = df[collist]
    for a in df_attr:
        gain = information_gain(df,a,target_attr)
        if gain > max_info_gain:
            max_info_gain = gain
            max_attr = a
    return max_attr

class Node():
    def __init__(self, attr, val, pred):
        self.attribute = attr
        self.children = {}
        self.prediction = pred

    def add_child(self, val, node):
        self.children[val] = node

    def remove_child(self,val):
        del self.children[val]

    def get_prediction(self):
        return self.prediction

    def get_attribute(self):
        return self.attribute

    def get_child(self, val):
        if val not in self.children:
            return self.children[min(self.children.keys(), key = lambda k: abs(k-val))]

        else:
            return self.children[val]


    def has_children(self):
        return len(self.children) != 0


#Adapted from the pseudocode provided in the in-class slides
def  id3(df, attrs, target_attr, default):
    unique_vals = df[target_attr].unique()

    if len(df) == 0:
        return Node(None, None, default)

    elif len(unique_vals) == 1:
        return Node(None, None, unique_vals[0])

    elif len(attrs) == 0:
        return Node(None, None, mode(df[target_attr]))

    else:
        best_attr = find_split(df, target_attr)
        root_node = Node(best_attr, "Root", None)
        for b in df[best_attr].unique():
            subset_df = df[df[best_attr] == b]
            attr_list = list(attrs)
            attr_list.remove(best_attr)
            sub_tree = id3(subset_df, attr_list, target_attr, df[target_attr].mode())
            root_node.add_child(b,sub_tree)
        return root_node

def predict(root_node, data_point, target_attr):
    current_node = root_node
    while current_node.has_children():
        attr = current_node.get_attribute()
        val = data_point[attr].iloc[0]
        current_node = current_node.get_child(val)

    return current_node.get_prediction()


def tree_accuracy(tree, test_df, target_attr):
    df_rows = len(test_df)
    count = 0
    for r in range(df_rows):
        data_point = test_df[r:(r+1)]
        prediction = predict(tree, data_point, target_attr)
        if prediction == data_point[target_attr].iloc[0]:
            count += 1
    accuracy = count/df_rows
    return accuracy



alg = id3(training_set, features, 'Class', choose_default)
print(alg)
acc = tree_accuracy(alg, test_set, 'Class')
print(acc)
