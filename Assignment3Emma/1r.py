import math
import operator
import pandas as pd
import numpy as np

column_names = ['Sepal Length', 'Sepal Width', 'Petal Length', 'Petal Width', 'Class']
data_set = pd.read_csv('iris.data.txt', header = None, names = column_names)

features = data_set.columns[:4]

training_set = data_set[0:40].append(data_set[50:90]).append(data_set[100:140])

test_set = data_set[40:50].append(data_set[90:100]).append(data_set[140:150])

training_default = training_set['Class']
most = training_default.mode()
choose_default = most[0]



def freq_of_classes(df, target_attr):
    val_freq = df[target_attr].value_counts()
    return val_freq

def most_freq_class(df,target_attr):
    mode = df[target_attr].mode()
    return mode

class Rule:
    def __init__(self, df, attr, attr_val, target_attr, target_attr_val):
        self.attr = attr
        self.attr_val = attr_val
        self.target_attr = target_attr
        self.target_attr_val = target_attr_val
        self.calc_accuracy(df)

    def calc_accuracy(self, df):
        subset_df = df[df[self.attr] == self.attr_val]
        subset_target = subset_df[subset_df[self.target_attr] == self.target_attr_val]
        acc = len(subset_target)/len(subset_df)
        self.accuracy = acc


    def get_accuracy(self):
        return self.accuracy


def find_split(df, attrs, target_attr):
    attr_rules = {}

    for a in attrs:
        unique_vals = df[a].unique()

        best_rule = None
        for val in unique_vals:

            most_freq = most_freq_class(df[df[a] == val], target_attr)

            if len(most_freq) == 0:
                most_freq = df[df[a] == val][target_attr].iloc[0]


            else:
                most_freq = most_freq.iloc[0]


            rule = Rule(df, a, val, target_attr, most_freq)
            acc_rule = rule.get_accuracy()
            if best_rule is None:
                best_rule = rule
                continue

            if acc_rule > best_rule.get_accuracy():
                best_rule = rule


        attr_rules[a] = best_rule

    best_attr_rule = None
    for a in attr_rules:
        rule = attr_rules[a]
        acc_attr = rule.get_accuracy()
        if best_attr_rule is None:
            best_attr_rule = rule
            continue

        if acc_attr > best_attr_rule.get_accuracy():
            best_attr_rule = rule

    return best_attr_rule.attr


class Node():
    def __init__(self, attr, val, pred):
        self.attribute = attr
        self.children = {}
        self.prediction = pred

    def add_child(self, val, node):
        self.children[val] = node

    def remove_child(self,val):
        del self.children[val]

    def get_prediction(self):
        return self.prediction

    def get_attribute(self):
        return self.attribute

    def get_child(self, val):
        if val not in self.children:
            return self.children[min(self.children.keys(), key = lambda k: abs(k-val))]

        else:
            return self.children[val]


    def has_children(self):
        return len(self.children) != 0


def one_r(df, attrs, target_attr, default):
    unique_vals = df[target_attr].unique()

    if len(df) == 0:
        return Node(None, None, default)

    elif len(unique_vals) == 1:
        return Node(None, None, unique_vals[0])

    elif len(attrs) == 0:
        return Node(None, None, mode(df[target_attr]))

    else:
        best_attr = find_split(df, attrs, target_attr)
        root_node = Node(best_attr, "Root", None)
        for b in df[best_attr].unique():
            subset_df = df[df[best_attr] == b]
            attr_list = list(attrs)
            attr_list.remove(best_attr)
            sub_tree = one_r(subset_df, attr_list, target_attr, df[target_attr].mode())
            root_node.add_child(b, sub_tree)
        return root_node

def predict(root_node, data_point, target_attr):
    current_node = root_node
    while current_node.has_children():
        attr = current_node.get_attribute()
        val = data_point[attr].iloc[0]
        current_node = current_node.get_child(val)

    return current_node.get_prediction()


def tree_accuracy(tree, test_df, target_attr):
    df_rows = len(test_df)
    count = 0
    for r in range(df_rows):
        data_point = test_df[r:(r+1)]
        prediction = predict(tree, data_point, target_attr)
        if prediction == data_point[target_attr].iloc[0]:
            count += 1
    accuracy = count/df_rows
    return accuracy

alg = one_r(training_set, features, 'Class', choose_default)

acc = tree_accuracy(alg, test_set, 'Class')

print(alg)
print(acc)
