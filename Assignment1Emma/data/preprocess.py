import re

#Taken from http://stackoverflow.com/questions/4576077/python-split-text-on-sentences
caps = "([A-Z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + caps + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + caps + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences

#Taken from http://stackoverflow.com/questions/13423919/computing-n-grams-using-python
def ngrams(string, n):
  string = string.split(' ')
  output = {}
  for i in range(len(string)-n+1):
    g = ' '.join(string[i:i+n])
    output.setdefault(g, 0)
    output[g] += 1
  return output

#Creates a database of sentences of ngrams.
def create_sentence_transactions(file_name,n):
    text_file = open(file_name,'r', encoding = 'utf-8')
    novel = text_file.read()
    novel = novel.replace(u'\ufeff', '')

    sentences = split_into_sentences(novel)
    ngrams_sentences = []
    for sentence in sentences:
        ng = set(list(ngrams(sentence,n).keys()))
        ngrams_sentences.append(ng)

    return ngrams_sentences

#Creates an itemset of ngrams
def create_ngram_list(database):
    ngram_dict = {}
    for sentence in database:
        for ngram in sentence:
            ngram_dict.setdefault(ngram,0)
    return list(ngram_dict.keys())
