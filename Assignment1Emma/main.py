from data import preprocess
import Apriori

database = preprocess.create_sentence_transactions('./data/PartIBookIChapterIandII.txt',2)
itemset = preprocess.create_ngram_list(database)

#Finds most frequent n-grams
frequent_itemsets = Apriori.findFrequentItemsets(itemset,database,3)
#Finds rules for n-grams
found_rules = Apriori.findRules(frequent_itemsets,database,1)

#Prints the frequent itemsets
visual_itemsets = Apriori.visualizeItemsets(frequent_itemsets)
#Prints the rules
visual_rules = Apriori.visualizeRules(found_rules)
