#Keeps track of how frequently n-grams appear
def support(itemset,database):
    frequency = 0
    for transaction in database:
        if itemset.issubset(transaction):
            frequency += 1
    return frequency

#Finds the confidence of rules
def confidence(precedent, antecedent, database):
    support_of_x = support(precedent,database)
    xy = set(precedent).union(set(antecedent))
    support_of_xy = support(xy,database)
    return support_of_xy/support_of_x

#Finds frequent itemsets
def findFrequentItemsets(itemset,database,minSupport):
    frequent_set = set()
    candidates = set(frozenset([item]) for item in itemset)
    while len(candidates) > 0:
        H = set()
        for c in candidates:
            if support(c,database) >= minSupport:
                H.add(c)
        candidates = set()
        for h in H:
            for item in H.difference(h):
                candidates.union((h.union(item)))
        frequent_set = frequent_set.union(H)
    return frequent_set

#Finds rules
def findRules(frequentItemsets,database,minConfidence):
    rules = []
    for s in frequentItemsets:
        for t in frequentItemsets.difference(s):
            if confidence(s,t,database) >= minConfidence:
                pair = (s,t)
                if s != t:
                    rules.append(pair)

    return rules


#Prints the frequent itemsets
def visualizeItemsets(frequentItemsets):
    print('These are the frequent item sets:')
    for fset in frequentItemsets:
        print(list(fset))
    print()

#Prints the rules
def visualizeRules(rules):
    print('These are the rules that were found:')
    for rule in rules:
        print(str(list(rule[0]))+ "===>"+str(list(rule[1])))
    print()
